package com.example.famouspersons;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import java.util.Random;

public class MainActivity extends AppCompatActivity {
    ImageView iv_Lincoln, iv_obama, iv_trump;
    RelativeLayout person1, person2, person3;
    Button inspiration;
    String[] myInspiration;
    EditText newDesc;
    Button editBtn;
    RadioGroup selectedPerson;
    TextView tv_lincoln_desc, tv_obama_desc, tv_trump_desc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeUI();
    }

    private void initializeUI() {
        // Inspiration string array
        myInspiration = getResources().getStringArray(R.array.inspiration);
        // Person pictures
        this.iv_Lincoln = (ImageView) findViewById(R.id.iv_lincoln);
        this.iv_obama = (ImageView) findViewById(R.id.iv_obama);
        this.iv_trump = (ImageView) findViewById(R.id.iv_trump);
        // Person boxes
        this.person1 = (RelativeLayout) findViewById(R.id.person1);
        this.person2 = (RelativeLayout) findViewById(R.id.person2);
        this.person3 = (RelativeLayout) findViewById(R.id.person3);
        // Inspiration BTN
        this.inspiration = (Button) findViewById(R.id.inspiration);
        // New description input box
        this.newDesc = (EditText) findViewById(R.id.et_desc);
        // Edit description BTN
        this.editBtn = (Button) findViewById(R.id.bt_edit_desc);
        // Radio group for selecting person
        this.selectedPerson = (RadioGroup) findViewById(R.id.rg_select_person);
        // Person descriptions
        this.tv_lincoln_desc = (TextView) findViewById(R.id.tv_lincoln_desc);
        this.tv_obama_desc = (TextView) findViewById(R.id.tv_obama_desc);
        this.tv_trump_desc = (TextView) findViewById(R.id.tv_trump_desc);
        // Hide first person on image click
        this.iv_Lincoln.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                person1.setVisibility(View.INVISIBLE);
            }
        });
        // Hide second person on image click
        this.iv_obama.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                person2.setVisibility(View.INVISIBLE);
            }
        });
        // Hide third person on image click
        this.iv_trump.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                person3.setVisibility(View.INVISIBLE);
            }
        });
        // On Inspiration BTN click
        this.inspiration.setOnClickListener(this::onClick);
        // On edit description BTN click
        this.editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch(selectedPerson.getCheckedRadioButtonId()) {
                    case R.id.rb_first:
                        setDescription(1, String.valueOf(newDesc.getText()));
                        break;
                    case R.id.rb_second:
                        setDescription(2, String.valueOf(newDesc.getText()));
                        break;
                    case R.id.rb_third:
                        setDescription(3, String.valueOf(newDesc.getText()));
                        break;
                }
            }
        });
    }

    // On Inspiration BTN click event call displayToast and display
    // a random message from the inspiration string array
    public void onClick(View view){
        this.displayToast(myInspiration[new Random().nextInt(4)]);
    }

    // Make toast message from a received string
    private void displayToast (String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    // Shortens description if it's too large or abort if it's empty.
    // Set new description depending on received id.
    public void setDescription(int id, String desc) {
        if (desc.length() > 65) {
            desc = desc.substring(0, 65) + "...";
        } else if (desc.length() <= 0) {
            displayToast(getResources().getString(R.string.desc_empty));
            return;
        }
        switch (id) {
            case 1:
                tv_lincoln_desc.setText(desc);
                break;
            case 2:
                tv_obama_desc.setText(desc);
                break;
            case 3:
                tv_trump_desc.setText(desc);
                break;
        }
    }
}