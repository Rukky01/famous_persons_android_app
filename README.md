# Famous_Persons_Android_App

#### This android app is created as a faculty project and as such has no real use-case just to show learned skill.
It features three famous persons, their names, date of birth, and a short description.
# Functionality
* On click on the Inspiration button, you get random Toast pop-up messages about some of them.
* Below, you have a radio group where you can select some person.
After that, you can enter a new description to replace the old one for that person.
* If the description is too long it gets shortened or if it's empty you get a Toast message.
* If you click on the image that person will disappear.

### Developed
This project is developed by [Luka Ruskan.](https://lukaruskan.com/)
### Preview
![Preview](preview.png)